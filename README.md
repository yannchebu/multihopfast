# README #

#This is a clustering protocol for multihop Iot Networks

#How to launch protocol
1. Step 1
		* Download InstantContiki3.0 OS on website https://sourceforge.net/projects/contiki/ 
		* Download and install Vmware Workstation https://my.vmware.com/fr/web/vmware/info/slug/desktop_end_user_computing/vmware_workstation_pro/12_0 
		* Unzip the OS Instantcontiki and open it (Instant_Contiki_Ubuntu_12.04_32-bit.vmx) with Vmware

2. Step 2

		*Copy this directory to /home/user/contiki/examples/ 
		*Go to directory /home/user/contiki/tools/cooja 
		*Open cooja simulaton on terminal with this command: ant run

3. Step 3

		*Create new simulation, add mote(Tmote sky or other), select file "multihop.c", complile it and Create. 
		*Choose the number of Nodes, Positionning... and start the simulation

