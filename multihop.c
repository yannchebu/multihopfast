/**
 * \file
 *         Instantanous Multihop clustering Algorithm for Iot
 * \author
 *         Yann Brice CHEBU MTOPI yannchebu@gmail.com
 */
#include "multihop.h"

/*---------------------------------------------------------------------------*/
PROCESS(multihop, "multihop");
AUTOSTART_PROCESSES(&multihop);
/*---------------------------------------------------------------------------*/
 
static struct broadcast_conn broadcast;

static void
broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from)
{
  //printf("broadcast message received from %d.%d: '%s'\n",
      //   from->u8[0], from->u8[1], (char *)packetbuf_dataptr());
  //printf("Bonjour je suis dans la fonction received. La valeur est %d \n\n",a);
   k = strlen((char *)packetbuf_dataptr());
   //printf("taille after receive %d\n",k);
  
  //if(end!=1){
    nbmsg ++;
    if(statut==-1){statut=0;}
    if(nbmsg == 1 && sendmessage==0){
		relay = from->u8[0];
		//printf("2 je suis CM dans ce cluster avec le relais %d \n",relay);
		statut=0;
		leds_toggle(LEDS_BLUE);
		if(sendmessage==0) {
			sendmessage = 1;
			// printf("je viens de recevoir un  message de %d\n",from->u8[0]);
			if(k>1){
				//printf("je fais une diffusion après un message de %d\n",from->u8[0]);
				packetbuf_copyfrom("Hello",k-1);
				broadcast_send(&broadcast);
				//leds_toggle(LEDS_RED);
				//leds_toggle(LEDS_RED);
			}   
			//~ else{
			//~ leds_toggle(LEDS_GREEN);  
			//~ }
		}
	}
}

static void
recv_uc(struct unicast_conn *c, const linkaddr_t *from)
{
  printf("unicast message received from %d.%d\n",
	 from->u8[0], from->u8[1]);
}
static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct unicast_conn uc;
static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(multihop, ev, data)
{
  static struct etimer et;
  
  PROCESS_EXITHANDLER(broadcast_close(&broadcast));

  PROCESS_BEGIN();
  etimer_set(&et, CLOCK_SECOND * 100 + random_rand() % (CLOCK_SECOND * 10));
			 PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
 powertrace_start(CLOCK_SECOND *0.5); // relever toute les 0.5 secondes
  broadcast_open(&broadcast, 129, &broadcast_call);
 
 
int pc=NUMBER_CH(ESTIMATION_NUMBER_C(ESTIMATION_NUMBER_C1(NUM_NODE),ESTIMATION_NUMBER_C2(NUM_NODE)),
                        NUM_NODE,
                        TOPOLOGY_AREA,
                        TRANSMISSION_RADIUS);
  //printf("la probabilité est %d \n\n",pc);
    //while(1) {

    /* Delay 2-4 seconds */
     //~ 
    beta = rand()%10;
     // printf(" %d \n",beta);
     //~ //etimer_set(&et,rand()%beta);
   //~ 
    packetbuf_copyfrom("Hello!", k);	
     int a;
     int t=CLOCK_REALTIME;
     
		a=random_rand()%20;
		 srand(node_id*2965);
         srand(28152);
     int test;
     //printf("le nombre générer est %d et seuil %d\n",a,seuil);  
	if(a > seuil) { 
	   etimer_set(&et, CLOCK_SECOND * node_id*10 + random_rand() % (CLOCK_SECOND));
       	   PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
	   if(sendmessage ==0){
			packetbuf_copyfrom("Hello!", k);
			broadcast_send(&broadcast);
			sendmessage = 1;
			statut = 1;
			printf(" je suis CH dans ce cluster \n");
			leds_toggle(LEDS_ALL);
			end = 1;
       } 
    }
    else{
		    // printf("je suis dans la phases de clusterhead pour les CM\n");
		     etimer_set(&et, CLOCK_SECOND*590);
       	   PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
		     if(sendmessage !=0){
			    statut = 0;
		        printf(" 1 je suis CM dans ce cluster avec le relais %d \n",relay);
		        
		     }
		     else{
			     etimer_set(&et, CLOCK_SECOND *5);
			     PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
			     if(sendmessage ==0){
					packetbuf_copyfrom("Hello!", k);
					broadcast_send(&broadcast);
					sendmessage = 1;
					statut = 1;
					printf(" je suis CH dans ce clusterrrrrrrrrrrrrr \n");
					leds_toggle(LEDS_ALL);
				 }
				 else{
				    statut = 0;
		            printf(" je suis CM dans ce cluster avec le relais %d \n",relay);	 
				 } 
				 
		      }		    
		}
powertrace_stop();
	
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
